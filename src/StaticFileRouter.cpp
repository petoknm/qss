#include <QtCore/QDir>
#include "qss/HttpResponse.h"
#include "qss/StaticFileRouter.h"

StaticFileRouter::StaticFileRouter(QDir root) : root(root) {
}

StaticFileRouter::operator RequestHandler() {
    return [root=root](HttpRequest &request, HttpResponse &response) {
        auto path = request.url.mid(1);
        QFile file(root.absoluteFilePath(path));

        if (file.open(QIODevice::ReadOnly)) {
            response.body = file.readAll();
            file.close();
        } else {
            response.body = "File not found";
            response.statusCode = 404;
        }
    };
}
