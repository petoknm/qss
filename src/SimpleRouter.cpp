#include "qss/HttpResponse.h"
#include "qss/SimpleRouter.h"

SimpleRouter::SimpleRouter() {
    notFound = [](HttpRequest &request, HttpResponse &response) {
        response = HttpResponse{404, "Not Found", "Route was not found"};
    };
}

void SimpleRouter::addRoute(Route route, RequestHandler requestHandler) {
    routes[route] = requestHandler;
}

void SimpleRouter::addRoute(HttpMethod method, QString url, RequestHandler requestHandler) {
    addRoute({method, url}, requestHandler);
}

SimpleRouter::operator RequestHandler() const {
    return [this](HttpRequest &request, HttpResponse &response) {
        Route route{request.method, request.url};
        if (routes.contains(route))
            routes[route](request, response);
        else
            notFound(request, response);
    };
}

void SimpleRouter::addNotFoundHandler(RequestHandler requestHandler) {
    notFound = requestHandler;
}

void SimpleRouter::onGet(QString url, RequestHandler requestHandler) {
    addRoute(HttpMethod::GET, url, requestHandler);
}

void SimpleRouter::onPost(QString url, RequestHandler requestHandler) {
    addRoute(HttpMethod::POST, url, requestHandler);
}

void SimpleRouter::onPut(QString url, RequestHandler requestHandler) {
    addRoute(HttpMethod::PUT, url, requestHandler);
}

void SimpleRouter::onDelete(QString url, RequestHandler requestHandler) {
    addRoute(HttpMethod::DELETE, url, requestHandler);
}
