#include "qss/HttpResponse.h"

HttpResponse::operator QByteArray() const {
    QByteArray result;

    result.append(QString("HTTP/1.1 %1 %2\r\n").arg(statusCode).arg(statusMessage));
    result.append(QString("Content-Type: %1\r\n").arg(contentType));
    result.append(QString("Content-Length: %1\r\n").arg(body.size()));
    result.append("Connection: close\r\n");

    for (auto i = headers.constBegin(); i != headers.constEnd(); ++i) {
        result.append(QString("%1: %2\r\n").arg(i.key()).arg(i.value()));
    }

    result.append("\r\n");

    result.append(body);

    return result;
}
