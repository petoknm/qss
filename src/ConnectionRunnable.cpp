#include <QtNetwork/QTcpSocket>
#include "qss/ConnectionRunnable.h"
#include "qss/HttpParser.h"

ConnectionRunnable::ConnectionRunnable(qintptr socketDescriptor, RequestHandler requestHandler)
        : socketDescriptor(socketDescriptor), requestHandler(requestHandler) {
}

void ConnectionRunnable::run() {
    QEventLoop loop;

    QTcpSocket *socket = new QTcpSocket();
    socket->setSocketDescriptor(socketDescriptor);

    HttpParser parser;

    QObject::connect(socket, &QAbstractSocket::disconnected, socket, &QObject::deleteLater);
    QObject::connect(socket, &QAbstractSocket::disconnected, &loop, &QEventLoop::quit);
    QObject::connect(socket, &QIODevice::readyRead, [&parser, socket]() {
        parser.parse(socket->readAll());
    });
    QObject::connect(&parser, &HttpParser::requestReady, [this, socket](HttpRequest request) {
        HttpResponse response;
        requestHandler(request, response);
        socket->write(response);
        socket->close();
    });

    loop.exec();
}