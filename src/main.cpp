#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>
#include "qss/HttpResponse.h"
#include "qss/StaticFileRouter.h"
#include "qss/ThreadedServer.h"
#include "qss/SimpleRouter.h"

int main(int argc, char **argv) {
    QCoreApplication application(argc, argv);
    QTextStream out(stdout, QIODevice::WriteOnly);
    QTextStream err(stderr, QIODevice::WriteOnly);
    application.setApplicationVersion("0.0.1");
    QCommandLineParser parser;

    parser.setApplicationDescription("QSS - Qt Simple Server");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addOption({"p", "Port used by the server", "port", "8000"});

    parser.process(application);

    QByteArray json = "{\"this\":\"is a json test\"}";

    SimpleRouter router;

    router.onGet("/", [](HttpRequest &request, HttpResponse &response) {
        response.body = "Hello World";
    });
    router.onGet("/test.json", [&](HttpRequest &request, HttpResponse &response) {
        response.body = json;
        response.contentType = "application/json";
    });
    router.onPost("/test.json", [&](HttpRequest &request, HttpResponse &response) {
        json = request.body;
        response.body = json;
        response.contentType = "application/json";
    });
    router.addNotFoundHandler(StaticFileRouter(QDir("../..")));

    ThreadedServer server(router);

    auto port = parser.value("p").toUShort();

    if (!server.listen(QHostAddress::Any, port)) {
        err << "QSS could not start on port " << port << "!\n" << flush;
        return -1;
    }

    out << "QSS listening on port: " << port << "\n" << flush;
    return application.exec();
}