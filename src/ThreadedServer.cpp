#include <QtCore/QThreadPool>
#include "qss/ThreadedServer.h"
#include "qss/ConnectionRunnable.h"

ThreadedServer::ThreadedServer(RequestHandler requestHandler)
        : requestHandler(requestHandler) {
}

void ThreadedServer::incomingConnection(qintptr socketDescriptor) {
    // The allocated ConnectionRunnable gets destroyed by the thread pool once it's finished
    auto *task = new ConnectionRunnable(socketDescriptor, requestHandler);
    QThreadPool::globalInstance()->start(task);
}