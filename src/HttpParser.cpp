#include "qss/HttpParser.h"

HttpParser::HttpParser() {
    http_parser_init(&parser, HTTP_REQUEST);
    parser.data = this;

    http_parser_settings_init(&settings);
    settings.on_message_complete = [](http_parser *parser) {
        HttpParser *self = reinterpret_cast<HttpParser *>(parser->data);
        self->messageCompleteCallback(static_cast<HttpMethod>(parser->method));
        return 0;
    };
    settings.on_url = [](http_parser *parser, const char *at, size_t length) {
        HttpParser *self = reinterpret_cast<HttpParser *>(parser->data);
        self->urlCallback(QString::fromUtf8(at, length));
        return 0;
    };
    settings.on_body = [](http_parser *parser, const char *at, size_t length) {
        HttpParser *self = reinterpret_cast<HttpParser *>(parser->data);
        QByteArray body(at, length);
        self->bodyCallback(body);
        return 0;
    };
    settings.on_header_field = [](http_parser *parser, const char *at, size_t length) {
        HttpParser *self = reinterpret_cast<HttpParser *>(parser->data);
        self->headerFieldCallback(QString::fromUtf8(at, length));
        return 0;
    };
    settings.on_header_value = [](http_parser *parser, const char *at, size_t length) {
        HttpParser *self = reinterpret_cast<HttpParser *>(parser->data);
        self->headerValueCallback(QString::fromUtf8(at, length));
        return 0;
    };
}

void HttpParser::parse(const QByteArray &data) {
    http_parser_execute(&parser, &settings, data.constData(), static_cast<size_t>(data.length()));
}

void HttpParser::messageCompleteCallback(HttpMethod method) {
    request.method = method;
    emit requestReady(request);
}

void HttpParser::urlCallback(QString url) {
    request.url = url;
}

void HttpParser::bodyCallback(QByteArray body) {
    request.body = body;
}

void HttpParser::headerFieldCallback(QString field) {
    if (lastWasValue) {
        request.headers[this->field] = this->value;
        this->field = field;
    } else {
        this->field += field;
    }
    lastWasValue = false;
}

void HttpParser::headerValueCallback(QString value) {
    if (lastWasValue) {
        this->value += value;
    } else {
        this->value = value;
    }
    lastWasValue = true;
}