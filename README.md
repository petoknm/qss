[![build status](https://gitlab.com/petoknm/qss/badges/master/build.svg)](https://gitlab.com/petoknm/qss/commits/master)
[![coverage report](https://gitlab.com/petoknm/qss/badges/master/coverage.svg)](https://gitlab.com/petoknm/qss/commits/master)

# QSS - Qt Simple Server

## Requirements and dependencies
Look at the Dockerfiles to see what is needed in order to build QSS

## Build Instructions
```shell
cmake .
make
```