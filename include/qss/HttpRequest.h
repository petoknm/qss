#ifndef QSS_HTTPREQUEST_H
#define QSS_HTTPREQUEST_H

#include <QtCore/QString>
#include <QtCore/QMap>

enum class HttpMethod {
    DELETE, GET, HEAD, POST, PUT, CONNECT, OPTIONS, TRACE
};

struct HttpRequest {
    HttpMethod method;
    QString url;
    QByteArray body;
    QMap<QString, QString> headers;
};

#endif //QSS_HTTPREQUEST_H
