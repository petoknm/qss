#ifndef QSS_ROUTE_H
#define QSS_ROUTE_H

#include "qss/HttpRequest.h"

struct Route {
    HttpMethod method;
    QString url;

    bool operator<(const Route &route) const {
        if (method == route.method) {
            return url < route.url;
        }
        return method < route.method;
    }
};

#endif //QSS_ROUTE_H
