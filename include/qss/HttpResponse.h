#ifndef QSS_HTTPRESPONSE_H
#define QSS_HTTPRESPONSE_H


#include <QtCore/QString>
#include <QtCore/QMap>

struct HttpResponse {
    int statusCode = 200;
    QString statusMessage = "OK";
    QString contentType = "text/plain";
    QByteArray body;
    QMap<QString, QString> headers;

    operator QByteArray() const;
};


#endif //QSS_HTTPRESPONSE_H
