#ifndef QSS_STATICFILEROUTER_H
#define QSS_STATICFILEROUTER_H


#include <QtCore/QDir>
#include "qss/RequestHandler.h"

class StaticFileRouter {
    QDir root;

public:
    StaticFileRouter(QDir root);

    operator RequestHandler();

};


#endif //QSS_STATICFILEROUTER_H
