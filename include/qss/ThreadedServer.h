#ifndef QSS_THREADEDSERVER_H
#define QSS_THREADEDSERVER_H

#include <QtNetwork/QTcpServer>
#include "qss/HttpRequest.h"
#include "qss/RequestHandler.h"

class ThreadedServer : public QTcpServer {

Q_OBJECT

    RequestHandler requestHandler;

public:
    ThreadedServer(RequestHandler requestHandler);

protected:
    void incomingConnection(qintptr socketDescriptor) override;

};


#endif //QSS_THREADEDSERVER_H
