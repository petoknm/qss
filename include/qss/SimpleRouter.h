#ifndef QSS_ROUTER_H
#define QSS_ROUTER_H


#include <QtCore/QMap>
#include "qss/HttpRequest.h"
#include "qss/RequestHandler.h"
#include "qss/Route.h"

class SimpleRouter {

    QMap<Route, RequestHandler> routes;
    RequestHandler notFound;

public:
    SimpleRouter();

    void addRoute(Route route, RequestHandler requestHandler);
    void addRoute(HttpMethod method, QString url, RequestHandler requestHandler);

    void onGet(QString url, RequestHandler requestHandler);
    void onPost(QString url, RequestHandler requestHandler);
    void onPut(QString url, RequestHandler requestHandler);
    void onDelete(QString url, RequestHandler requestHandler);

    void addNotFoundHandler(RequestHandler requestHandler);

    operator RequestHandler() const;
};


#endif //QSS_ROUTER_H
