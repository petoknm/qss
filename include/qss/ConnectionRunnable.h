#ifndef QSS_CONNECTIONRUNNABLE_H
#define QSS_CONNECTIONRUNNABLE_H


#include <QtCore/QRunnable>
#include <QtCore/QEventLoop>
#include "qss/HttpRequest.h"
#include "qss/RequestHandler.h"

class ConnectionRunnable : public QRunnable {

    qintptr socketDescriptor;
    RequestHandler requestHandler;

public:
    ConnectionRunnable(qintptr socketDescriptor, RequestHandler requestHandler);
    void run() override;

};


#endif //QSS_CONNECTIONRUNNABLE_H
