#ifndef QSS_REQUESTHANDLER_H
#define QSS_REQUESTHANDLER_H

#include <functional>
#include <QtCore/QByteArray>
#include "qss/HttpRequest.h"
#include "qss/HttpResponse.h"

typedef std::function<void(HttpRequest &, HttpResponse &)> RequestHandler;

#endif //QSS_REQUESTHANDLER_H
