#ifndef QSS_HTTPPARSER_H
#define QSS_HTTPPARSER_H

#include <QtCore/QByteArray>
#include <http_parser.h>
#include <QtCore/QObject>
#include "qss/HttpRequest.h"

class HttpParser : public QObject {

Q_OBJECT

    http_parser parser;
    http_parser_settings settings;
    HttpRequest request;
    bool lastWasValue = false;
    QString field;
    QString value;

public:
    HttpParser();
    void parse(const QByteArray &data);

signals:
    void requestReady(HttpRequest request);

private:
    void messageCompleteCallback(HttpMethod method);
    void urlCallback(QString url);
    void bodyCallback(QByteArray body);
    void headerFieldCallback(QString field);
    void headerValueCallback(QString value);

};


#endif //QSS_HTTPPARSER_H
