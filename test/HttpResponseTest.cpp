#include <QtCore/QByteArray>
#include "gtest/gtest.h"
#include "qss/HttpResponse.h"

TEST(HttpResponseTest, SerializeStatusCode){
    HttpResponse response;
    response.statusCode = 213;
    QByteArray serialized = response;
    EXPECT_TRUE(serialized.contains("HTTP/1.1 213 "));
}

TEST(HttpResponseTest, SerializeStatusMessage){
    HttpResponse response;
    response.statusMessage = "ThisIsTestStatusMessage";
    QByteArray serialized = response;
    EXPECT_TRUE(serialized.contains("HTTP/1.1 200 ThisIsTestStatusMessage"));
}

TEST(HttpResponseTest, SerializeContentType){
    HttpResponse response;
    response.contentType = "test/made-up-content-type";
    QByteArray serialized = response;
    EXPECT_TRUE(serialized.contains("Content-Type: test/made-up-content-type"));
}

TEST(HttpResponseTest, SerializeBody){
    HttpResponse response;
    QByteArray body = "Test body, Test body, Test body, Test body, Test body";
    response.body = body;
    QByteArray serialized = response;
    EXPECT_TRUE(serialized.contains(body));
}

TEST(HttpResponseTest, SerializeHeaders){
    HttpResponse response;
    response.headers["Accept"] = "text/plain";
    response.headers["Test-Header"] = "some-value";
    QByteArray serialized = response;
    EXPECT_TRUE(serialized.contains("Accept: text/plain"));
    EXPECT_TRUE(serialized.contains("Test-Header: some-value"));
}