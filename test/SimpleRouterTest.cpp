#include <gtest/gtest.h>
#include <qss/SimpleRouter.h>

TEST(SimpleRouterTest, AddRouteByParts) {
    SimpleRouter router;
    router.addRoute(HttpMethod::GET, "/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::GET, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, AddRoute) {
    SimpleRouter router;
    router.addRoute({HttpMethod::GET, "/this_is_a_test.html"}, [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::GET, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, OnGet) {
    SimpleRouter router;
    router.onGet("/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::GET, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, OnPost) {
    SimpleRouter router;
    router.onPost("/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::POST, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, OnPut) {
    SimpleRouter router;
    router.onPut("/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::PUT, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, OnDelete) {
    SimpleRouter router;
    router.onDelete("/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::DELETE, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, Return404WhenNotFound) {
    SimpleRouter router;
    RequestHandler handler = router;
    HttpRequest request{HttpMethod::GET, "/this_is_a_test.html"};
    HttpResponse response;
    handler(request, response);
    EXPECT_TRUE(QByteArray(response).contains("HTTP/1.1 404 Not Found"));
}

TEST(SimpleRouterTest, ExecuteRouteIfMatched) {
    SimpleRouter router;
    router.addRoute(HttpMethod::GET, "/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::GET, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}

TEST(SimpleRouterTest, ExecuteRouteIfPathAndMethodMatched) {
    SimpleRouter router;
    router.addRoute(HttpMethod::POST, "/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });
    router.addRoute(HttpMethod::GET, "/this_is_a_test.html", [](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(true);
    });
    router.addNotFoundHandler([](HttpRequest &request, HttpResponse &response) {
        EXPECT_TRUE(false);
    });

    HttpRequest request{HttpMethod::GET, "/this_is_a_test.html"};
    HttpResponse response;
    RequestHandler handler = router;
    EXPECT_NO_FATAL_FAILURE(handler(request, response));
}